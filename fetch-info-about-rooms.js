const assert = require('assert');
const path = require('path');
const fs = require('fs');
const fetch = require('node-fetch');
const LineByLineReader = require('line-by-line');

const secrets = require('./secrets.json');
assert(secrets.accessToken);

//const BASE_URL = 'http://localhost:18008';
const BASE_URL = 'https://gitter.ems.host';

const FAILED_LOG_PATH = path.resolve('/tmp/failed-matrix-rooms.db');
const ROOM_INFO_PATH = path.resolve('./room-info.ndjson');

async function joinMatrixRoom(matrixRoomId) {
  // https://matrix.org/docs/spec/client_server/latest#get-matrix-client-r0-rooms-roomid-aliases
  //const joinEndpoint = `${BASE_URL}/_matrix/client/r0/rooms/${matrixRoomId}/join`;
  const joinEndpoint = `${BASE_URL}/_matrix/client/r0/join/${matrixRoomId}?server_name=matrix.org`;

  const res = await fetch(joinEndpoint, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${secrets.accessToken}`,
      'Content-Type': 'application/json',
    },
  });
  const data = await res.json();
  if (res.status !== 200) {
    throw new Error(
      `Non-200 response (got ${res.status}) from ${joinEndpoint}: ${JSON.stringify(data)}`
    );
  }

  return data.name;
}

async function fetchStateForMatrixRoom(matrixRoomId, stateKey) {
  // https://matrix.org/docs/spec/client_server/latest#get-matrix-client-r0-rooms-roomid-state-eventtype-statekey
  const stateEndpoint = `${BASE_URL}/_matrix/client/r0/rooms/${matrixRoomId}/state/${stateKey}/`;

  const res = await fetch(stateEndpoint, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${secrets.accessToken}`,
      'Content-Type': 'application/json',
    },
  });
  const data = await res.json();
  if (res.status !== 200) {
    throw new Error(
      `Non-200 response (got ${res.status}) from ${stateEndpoint}: ${JSON.stringify(data)}`
    );
  }

  return data;
}

async function fetchInfoForRoom(roomEntry) {
  const matrixRoomId = roomEntry.matrix_id;

  let name;
  let aliases = {};
  try {
    await joinMatrixRoom(matrixRoomId);

    ({ name } = await fetchStateForMatrixRoom(matrixRoomId, 'm.room.name'));
    console.log('name', name);

    aliases = await fetchStateForMatrixRoom(matrixRoomId, 'm.room.canonical_alias');
    console.log('aliases', aliases);
  } catch (err) {
    console.error(err, err.stack);
    fs.appendFileSync(FAILED_LOG_PATH, JSON.stringify(roomEntry) + '\n');
  }

  return {
    gitterRoomUri: roomEntry.remote_id,
    matrixRoomId,
    name,
    alias: aliases.alias,
    alt_aliases: aliases.alt_aliases,
  };
}

async function processLine(line) {
  const roomEntry = JSON.parse(line);

  console.log('Processing roomEntry', roomEntry);

  if (!roomEntry.matrix_id) {
    console.log(`Skipping entry because no matrix_id present ${roomEntry}`);
    return;
  }

  const info = await fetchInfoForRoom(roomEntry);
  fs.appendFileSync(ROOM_INFO_PATH, JSON.stringify(info) + '\n');
}

(async () => {
  try {
    fs.writeFileSync(FAILED_LOG_PATH, '');
  } catch (err) {
    console.log('Failed to create the log file for failed rooms');
    throw err;
  }

  const lr = new LineByLineReader('./room-store.db');

  lr.on('error', (err) => {
    console.error('Error while reading lines', err);
  });

  lr.on('line', async (line) => {
    lr.pause();

    try {
      await processLine(line);

      await new Promise((resolve) => {
        setTimeout(resolve, 2000);
      });
    } catch (err) {
      console.error(err, err.stack);
      fs.appendFileSync(FAILED_LOG_PATH, line + '\n');
    }

    lr.resume();
  });

  lr.on('end', () => {
    console.log('All lines processed');
  });
})();
